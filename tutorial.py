import math
import numpy as np
from functools import partial
from qaoa_functions import (get_groundstate, standard_parity_circuits, cost_function_parity_decoding,
                            rerouting_circuits, cost_function_logical, create_noise_model,
                            QAOASimulator, create_complete_graph_and_parity_constraints)

# N = number of logical qubits
num_logical_qubits = 4
# number of QAOA layers
num_qaoa_layers = 2
# for optimization: number of random initializations (of the start parameters)
number_of_initializations = 3
# seed for parameter initialization - for reproducibility, or None
seed_for_init = 312
# error rates
one_qubit_gate_error = 0.
two_qubit_gate_error = 0.
# number of shots/measurements
number_measurement = 1000

# K = number of physical qubits/ interactions
num_physical_qubits = num_logical_qubits * (num_logical_qubits - 1) // 2

# create random interaction strengths, with elements in {-1, -0.9,.. -0.1, 0.1, ....  0.9, 1}
values = np.linspace(-10, 10, 21) / 10
values = np.delete(values, np.where(values == 0))
jij = np.random.choice(values, num_physical_qubits)  # default probabilities is a uniform distribution

# logical graph is defined by a list of the edges [(node_x, node_y), (), ...]
# logical graph is a complete graph
# constraints in the parity embedding: group of integers are indices to the edges in the logical graph
logical_graph, constraints = create_complete_graph_and_parity_constraints(num_logical_qubits)
# logical lines are the spanning trees used for decoding in the parity approach
spanning_trees = [[tuple(sorted((a, b))) for a in range(num_logical_qubits) if a != b] for b in range(num_logical_qubits)]

# create depolarizing noise channel, if error rates are 0. functions returns None
noise = create_noise_model(one_qubit_gate_error, two_qubit_gate_error)

print('------------SIMULATION DETAILS-------------')
print(f'#logical qubits: {num_logical_qubits}, QAOA layers: {num_qaoa_layers}, interaction strengths: {jij}')
print(f'2-qubit gate error rate: {two_qubit_gate_error}, 1-qubit gate error rate: {one_qubit_gate_error}')
print(f'Optimization: bfgs-search, #random initializations: {number_of_initializations}, seed: {seed_for_init}')
print('-------')

for QAOA_approach in ['rerouting', 'parity']:
    # create QAOA circuits depending on the QAOA approach
    if QAOA_approach == 'parity':
        # programstring, to build a complete circuit of the subcircuits
        # Z: local field circuit, C: constraint circuits, X: driver circuit
        pstr = 'ZCX' * num_qaoa_layers
        # length of one cycle/number of different subcircuits,
        # needed to adapt parameter range of the local fields Z
        len_cyc = 3
        qubit_number = num_physical_qubits
        subcircs = standard_parity_circuits(num_physical_qubits, jij, constraints)
        # objective function for the optimizer
        cost_function = partial(cost_function_parity_decoding, logical_graph, jij, spanning_trees, 'mean')
    elif QAOA_approach == 'rerouting':
        # programstring: to build a complete circuit of the subcircuits
        # P: interaction circuit, X: driver circuit
        pstr = 'PX' * num_qaoa_layers
        # length of one cycle/number of different subcircuits,
        # needed to adapt parameter range of the interactions P
        len_cyc = 2
        qubit_number = num_logical_qubits
        subcircs = rerouting_circuits(num_logical_qubits, jij, logical_graph)
        # objective function for the optimizer
        cost_function = partial(cost_function_logical, logical_graph, jij)
    else:
        raise NotImplementedError(f'QAOA approach \"{QAOA_approach}\" not known, try \"parity\" or \"rerouting\".')

    # Parameter ranges: (0, 2pi), 2pi because qiskit has a factor of 1/2 in the gate definitions
    # If values in the Jij array are not \pm 1 then the parameter range for P and Z is not limited to 2pi
    # We need to get the greatest common divisor of the Jij array to adjust parameter range
    gcd = [math.gcd(int(jij[k + 2] * 10), math.gcd(int(jij[k] * 10), int(jij[k + 1] * 10))) / 10
           for k in range(len(jij) - 2)]
    parameter_range_factor_interactions = min(gcd)
    parameter_range = [(0, 2 * np.pi)] * len(pstr)
    for nr_cyc in range(num_qaoa_layers):
        parameter_range[(nr_cyc - 1) * len_cyc] = (0, 2 / parameter_range_factor_interactions * np.pi)

    # set up QAOA with bfgs search as classical optimization routine
    q = QAOASimulator(qubit_number, pstr, cost_function, subcircs, num_shots=number_measurement, noise_model=noise)
    res = q.repeated_bfgs_search(parameter_bounds=parameter_range, number_of_initializations=number_of_initializations, seed=seed_for_init)
    # Count number of CNOT gates in the circuit.
    # Quality depends on circuit transpilation.
    # Good numbers for N=4/5/6 are 15/29/45 CNOT gates per layer.
    number_of_CNOTs_decomposed = q.circuit.decompose().decompose().decompose().count_ops()['cx']

    # get results from the QAOA
    energies = []
    fidelities = []
    # For parity we optimize with the mean energy, but to be able to compare the energies
    # in the end, we will change the energy function to the minimum energy
    if QAOA_approach == 'parity':
        cost_function = partial(cost_function_parity_decoding, logical_graph, jij, spanning_trees, 'min')
        q = QAOASimulator(qubit_number, pstr, cost_function, subcircs, num_shots=number_measurement, noise_model=noise)
    # get results from all initializations
    for k in range(number_of_initializations):
        used_params = res[k].parameters
        res_state_vec = q.execute_circuit(used_params)
        # energy
        energies.append(q.energy_from_counts(res_state_vec.get_counts()))
        # groundstate probability/fidelity
        groundstate = get_groundstate(num_logical_qubits, logical_graph, jij)
        state_from_simulation = res_state_vec.get_counts()

        if QAOA_approach == 'rerouting':
            fidelity = q.get_fidelity(groundstate, state_from_simulation)
        else:
            fidelity = q.get_logical_fidelity_with_spanning_tree(groundstate, state_from_simulation, spanning_trees,
                                                                 logical_graph)
        fidelities.append(fidelity)

    # return result with lowest energy
    index_lowest_energy = energies.index(min(energies))
    fidel_of_lowest_energy = fidelities[index_lowest_energy]
    print(f'QAOA approach: {QAOA_approach}, '
          f'#CNOTs: {number_of_CNOTs_decomposed}')
    print(f'Lowest Energy: {np.round(min(energies), 4)}, '
          f'Success Probability: {np.round(fidel_of_lowest_energy, 3)}')
    print('-------')
