from setuptools import setup

setup(
    name='Error mitigation in parity quantum approximate optimization',
    version='1.0',
    python_requires='>=3.8',
    install_requires=[
        'jupyter',
        'qiskit>=0.22.0'
    ],
    author='anita',
    author_email='anita.weidinger@uibk.ac.at',
)
