from typing import Dict, Callable, Optional, List, Tuple
import numpy as np
from qiskit.providers.aer.noise import depolarizing_error, NoiseModel
from qiskit import QuantumCircuit, Aer, execute, transpile
from qiskit.transpiler import CouplingMap
from qiskit.circuit import Parameter, ParameterVector
from dataclasses import dataclass


@dataclass
class Result:
    parameters: np.array
    final_objective_value: float


class QAOASimulator:
    def __init__(self, n_qubits: int,
                 programstring: str,
                 energy_function: Callable,
                 subcircuits: Dict[str, QuantumCircuit],
                 num_shots: int = 10000,
                 noise_model: Optional[NoiseModel] = None):
        """

        :param n_qubits: number of qubits in the circuit
        :param programstring: sequence of subcircuits to be used for QAOA,
               e.g., ZCX (Z: local field circuit, C: constraint circuit, X: driver circuit)
        :param energy_function: takes binary representation returned from qiskit to calculate energy
        :param subcircuits: parametrized subcircuits where each circuit is for a given letter from the programstring
        :param num_shots: number of repetitions for qiskit circuit execution
        :param noise_model: qiskit noise model
        """
        self.n_qubits = n_qubits
        self.energy_function = energy_function
        self.subcircuits = subcircuits
        self.num_shots = num_shots
        self.noise_model = noise_model

        self.circuit, self.circuit_parameters = self.create_circuit(programstring)
        self.backend = Aer.get_backend('qasm_simulator')

    def energy_from_counts(self, counts: Dict[str, int]) -> float:
        """
        :param counts: Dict from execute_circuit(params).get_counts();
               keys: binary configuration, values: measured amount
        :return: expectation value of the energy
        """
        total = 0
        measured_energy = 0
        for binary_configuration, counts in counts.items():
            measured_energy += self.energy_function(binary_configuration) * counts
            total += counts
        return measured_energy / total

    def execute_circuit(self, parameters: List[float]):
        """
        Executes the circuit with the given input parameters

        :param parameters: Contains circuit parameters
        :return: returns qiskit result object of the circuit execution with the given input parameters
        """

        bound_circuit = self.circuit.bind_parameters(
            {par: val for par, val in zip(self.circuit_parameters, parameters)})
        executed_job = execute(bound_circuit, self.backend,
                               noise_model=self.noise_model, shots=self.num_shots)

        result = executed_job.result()
        return result

    def create_circuit(self, programstring: str) -> Tuple[QuantumCircuit, ParameterVector]:

        circuit_parameters = ParameterVector('p', length=len(programstring))
        circuit = QuantumCircuit(self.n_qubits)
        # initialize all qubits in |+>
        circuit.h(range(self.n_qubits))

        for letter, parameter in zip(programstring, circuit_parameters):
            instruction = self.subcircuits[letter].to_instruction(
                {original_param: parameter
                 for original_param in self.subcircuits[letter].parameters})

            circuit.append(instruction, range(self.n_qubits))

        circuit.measure_all(inplace=True)

        if 'P' in programstring:
            circuit = transpile_circuit(self.n_qubits, circuit)

        return circuit, circuit_parameters

    def repeated_bfgs_search(self, parameter_bounds: List,
                             number_of_initializations: int = 1000,
                             number_of_iterations_per_initialization: int = 1000,
                             seed: int = None) -> List[Result]:
        """
        Chooses n_repeat random points in the parameter space and tries to find the local optimum with
        these random points as initial parameters, using the bfgs method of scipy. The global optimum is
        assumed to be the best result from the local optima.

        :param parameter_bounds: The ranges the parameters take. Must be given as an array of 2-tuples.
               The order of the ranges must match the order of the parameter letters in the program string.
        :param number_of_initializations: The number of repetitions/initializations of the algorithm (integer).
        :param number_of_iterations_per_initialization: Maximal number of steps the optimizer does per initialization
        :param seed: Random seed. Optional, in order to make the search deterministic (i. e. the results
               are reproducible).
        :return: Instances of Result in a List
        """
        from scipy import optimize

        def minf(params):
            return self.energy_from_counts(self.execute_circuit(params).get_counts())

        np.random.seed(seed)
        results = []
        for _ in range(number_of_initializations):
            initial_parameters = np.array([np.random.uniform(*par_range) for par_range in parameter_bounds])
            scipy_res = optimize.minimize(minf, initial_parameters, method='l-bfgs-b', bounds=parameter_bounds,
                                          options={'maxfun': number_of_iterations_per_initialization})
            results.append(Result(parameters=scipy_res.x, final_objective_value=scipy_res.fun))

        return results


    @staticmethod
    def get_fidelity(groundstates: List,
                     state: Dict[str, int]) -> float:
        """
        Returns ground state fidelity of a state.

        :param groundstates: groundstate(s) in a list. Format as returned by get_groundstate()
        :param state: The state for which the groundstate-fidelity shall be determined.
        :return: Groundstate fidelity of the decoded physical state. Here, it is defined as the probability
                 to measure system in the groundstate when the system is in the state 'state'.
        """
        fidelity = 0.
        gstates = groundstates.copy()
        # normalize state
        total_counts = sum(state.values())
        for key in state.keys():
            state[key] /= total_counts

        # convert gs array into qiskit bitstring notation. Replacement: -1->1 and 1->0, little endian
        for groundstate in gstates:
            groundstate_str = ''.join(['0' if b == 1 else '1' for b in groundstate[::-1]])

            if groundstate_str in state.keys():
                fidelity += state[groundstate_str]
        return fidelity


    @staticmethod
    def get_logical_fidelity_with_spanning_tree(groundstates: List,
                                                state: Dict[str, int],
                                                trees_for_decoding: List[List[Tuple]],
                                                interactions: List[Tuple]) -> float:
        """
        Returns ground state fidelity of a physical state 'state' which is decoded into a logical state with
        spanning trees of the logical graph.

        :param groundstates: logical groundstate(s) in a list. Format as returned by get_true_gs()
        :param state: The physical state for which we can determine the counts
               Format is a dictionary with counts, as returned by
               qiskit.result.result.Result.get_counts()
        :param trees_for_decoding: input graph, for example [(0, 1), (1, 2), (2, 0)],
               format as from random_spanning_trees
               From the tree we can determine the corresponding logical state
        :param interactions: The logical graph as an array of edges, for example [(0, 1), (1, 2), (2, 0)]
        :return: Groundstate fidelity of the decoded physical state. Here, it is defined as the probability
                 to measure system in the groundstate when the system is in the state 'state'.
        """
        fidelity = 0.
        gstates = groundstates.copy()
        # normalize state
        total_counts = sum(state.values())
        for key in state.keys():
            state[key] /= total_counts

        for physical_state in state.keys():
            logical_str_list = []
            for tree in trees_for_decoding:
                # gets logical state of spanning tree
                logical_str_list.append(physical_to_logical(interactions, tree, physical_state[::-1]))

            # convert groundstate array into qiskit bitstring notation. Replacement: -1->1 and 1->0, little endian
            for groundstate in gstates:
                groundstate_str = ''.join(['0' if b == 1 else '1' for b in groundstate])

                if groundstate_str in logical_str_list:
                    fidelity += state[physical_state]
                    break
        return fidelity


def cost_function_parity_decoding(interactions: List[Tuple[int, int]],
                                  interaction_strengths: List[float],
                                  trees_for_decoding: List[List[Tuple[int, int]]],
                                  mean_or_min: str,
                                  bin_conf: str) -> float:
    """
    Returns logical energy of a physical configuration based on the spanning trees that are used for
    decoding. It returns either the mean energy or the minimal energy of all decoded configurations.

    :param interactions: The logical graph as an array of edges, for example [(0, 1), (1, 2), (2, 0)]
    :param interaction_strengths: interaction strengths (or local fields in the parity encoding),
           for example [1., -1., -1.].
    :param trees_for_decoding: spanning trees used for decoding
    :param mean_or_min: can either be 'mean' or 'min', depending if one wants to return
           the mean or the min of all decoded energies
    :param bin_conf: physical configuration, expects bitstring in qiskit format - little endian
    :return: float, energy of bin_conf
    """
    if mean_or_min not in ['mean', 'min']:
        raise NotImplementedError(f'Can either return the \"mean\" or \"min\" energy, not: {mean_or_min}')

    logical_energy_from_trees = []
    count = 0
    for tree in trees_for_decoding:
        # gets logical state of spanning tree (reverse bin_conf as qiskit uses little endian)
        logical_state_string = physical_to_logical(interactions, tree, bin_conf[::-1])
        # gets energy of logical state (reverse bin_conf as function expects qiskit argument)
        logical_energy_from_trees.append(cost_function_logical(interactions, interaction_strengths,
                                                               logical_state_string[::-1]))
        count += 1
    # calculate mean or min energy
    if mean_or_min == 'mean':
        energy = sum(logical_energy_from_trees) / count
    else:
        energy = min(logical_energy_from_trees)
    return energy


def cost_function_logical(interactions: List[Tuple[int, int]],
                          interaction_strengths: List[float],
                          configuration_in_binary: str) -> float:
    """
    Returns logical energy of logical configuration.

    :param interactions: The logical graph as an array of edges, for example [(0, 1), (1, 2), (2, 0)]
    :param interaction_strengths: interaction strengths, for example [1, -1, -1].
    :param configuration_in_binary: logical configuration, expects bitstring in qiskit format - little endian
    :return: float, energy of bin_conf
    """
    configuration = [1 if b == '0' or b == 0 else -1 for b in configuration_in_binary[::-1]]
    energy = 0
    for i, co in enumerate(interactions):
        energy += interaction_strengths[i] * configuration[co[0]] * configuration[co[1]]
    return energy


def physical_to_logical(interactions: List[Tuple[int, int]],
                        tree: List[Tuple[int, int]],
                        physical_configuration: str) -> str:
    """
    Returns logical state to a given physical configuration and spanning tree.

    :param interactions: The logical graph as an array of edges, for example [(0, 1), (1, 2), (2, 0)]
    :param tree: same structure as interactions: [(0, 1), (1, 2)] for example.
    :param physical_configuration: a string, eg '001100', for 6 physical qubits
           does NOT expect qiskit format - big endian
    :return: string, logical state of the given physical configuration + tree - big endian
    """
    edges_in_tree = tree.copy()
    edge_to_state = {edge: 1 - 2 * int(state)
                     for edge, state in zip(interactions, physical_configuration)}
    root = tree[0][0]
    logical_dict = {root: 1}
    while edges_in_tree:
        for edge in edges_in_tree:
            if edge[0] in logical_dict:
                logical_dict[edge[1]] = logical_dict[edge[0]] * edge_to_state[edge]
                edges_in_tree.remove(edge)
                break
            elif edge[1] in logical_dict:
                logical_dict[edge[0]] = logical_dict[edge[1]] * edge_to_state[edge]
                edges_in_tree.remove(edge)
                break

    return ''.join(str((1 - logical_dict[x]) // 2) for x in sorted(logical_dict))


def driver_circuit(number_of_qubits: int) -> QuantumCircuit:
    """
    :param number_of_qubits: number of qubits in the driver circuit
    :return: driver circuit
    """
    circuit = QuantumCircuit(number_of_qubits)
    circuit.rx(Parameter('pX'), qubit=range(number_of_qubits))
    circuit.name = 'rotationX'
    return circuit


def local_field_circuit(interaction_strengths: List[float]) -> QuantumCircuit:
    """
    :param interaction_strengths: interaction strengths (local field in parity encoding)
    :return: local field circuit
    """
    circuit = QuantumCircuit(len(interaction_strengths))

    param = Parameter('pZ')
    for i, field in enumerate(interaction_strengths):
        circuit.rz(param * field, qubit=i)

    circuit.name = 'local_fields'
    return circuit


def interaction_circuit(number_of_qubits: int,
                        interaction_strengths: List[float],
                        interactions: List[Tuple]) -> QuantumCircuit:
    """
    :param number_of_qubits: number of qubits with interactions
    :param interaction_strengths: interaction strengths
    :param interactions: The logical graph as a list of edges, for example [(0, 1), (1, 2), (2, 0)]
    :return: interaction circuit
    """
    circuit = QuantumCircuit(number_of_qubits)
    param = Parameter('pP')
    assert len(interaction_strengths) == len(interactions), "Number of couplings does not coincide with length of jij!"
    for i, int_i in enumerate(interactions):
        # consider higher order interactions
        for j in range(len(int_i) - 1):
            circuit.cx(int_i[j], int_i[j + 1])
        circuit.rz(interaction_strengths[i] * param, qubit=int_i[-1])
        for j in range(len(int_i) - 1)[::-1]:
            circuit.cx(int_i[j], int_i[j + 1])
    circuit.name = 'interactions'
    return circuit


def single_constraint_circuit(number_of_constraint_qubits: int) -> Tuple[QuantumCircuit, Parameter]:
    """
    Returns single constraint circuit which consists of
    a chain of CNOT gates, z-rotations on the last qubit in the chain, another chain of CNOTs reversed.
    For more information see: https://doi.org/10.1109/TQE.2020.3034798

    :param number_of_constraint_qubits: number of qubits in the constraint
    :return: single constraint circuit
    """
    c = Parameter('c')
    circuit = QuantumCircuit(number_of_constraint_qubits, name='co')
    qubits = list(range(number_of_constraint_qubits))

    chain = QuantumCircuit(number_of_constraint_qubits)
    for i in range(number_of_constraint_qubits - 1):
        chain.cx(i, i + 1)

    circuit.append(chain.to_instruction(), range(number_of_constraint_qubits))
    circuit.rz(phi=c, qubit=qubits[-1])
    circuit.append(chain.inverse().to_instruction(), range(number_of_constraint_qubits))

    return circuit, c


def constraint_circuit(number_of_qubits: int,
                       parity_constraints: List[List[int]]) -> QuantumCircuit:
    """
    Returns complete constraint circuit, consisting of several 'single_constraint_circuit(n_qubits)'
    For more information about parity constraints see: https://doi.org/10.1109/TQE.2020.3034798

    :param number_of_qubits: number of (physical) qubits
    :param parity_constraints: list of all constraints
    :return: constraint circuit
    """
    circuit = QuantumCircuit(number_of_qubits)
    parameter = Parameter('pC')

    for j, constraint in enumerate(parity_constraints):
        temp_co, param = single_constraint_circuit(len(constraint))
        circuit.append(temp_co.to_instruction({param: parameter}), constraint)

    circuit.name = 'constraints'
    return circuit


def standard_parity_circuits(number_of_qubits: int,
                             interaction_strengths: List[float],
                             parity_constraints: List[List[int]]) -> Dict[str, QuantumCircuit]:
    """
    Returns parameterized parity (LHZ) QAOA circuits

    :param number_of_qubits: number of physical qubits
    :param interaction_strengths: of the logical graph = local fields in the parity encoding
    :param parity_constraints: list of all constraints
    :return: dictionary of subcircuits where each circuit is for a given letter from the programstring
    """
    return {'X': driver_circuit(number_of_qubits),
            'Z': local_field_circuit(interaction_strengths),
            'C': constraint_circuit(number_of_qubits, parity_constraints)}


def rerouting_circuits(n_qubits: int,
                       interaction_strengths: List[float],
                       interactions: List[Tuple]) -> Dict[str, QuantumCircuit]:
    """
    Returns parameterized QAOA rerouting circuits

    :param n_qubits: number of (logical) qubits
    :param interaction_strengths: interaction strengths/Jij matrix
    :param interactions: The logical graph as an array of edges, for example [(0, 1), (1, 2), (2, 0)]
    :return: dictionary of subcircuits where each circuit is for a given letter from the programstring
    """
    return {'X': driver_circuit(n_qubits),
            'P': interaction_circuit(n_qubits, interaction_strengths, interactions)}


def transpile_circuit(n_qubits: int,
                      circuit_to_compile: QuantumCircuit,
                      seed: int = None) -> QuantumCircuit:
    """
    Compiles a circuit to a chip with quadratic topology
    uses the qiskit transpiler
    only implemented for 4, 5 and 6 qubits
    0----1
    |    |
    2----3
    |    |
    4----5

    :param n_qubits: number of qubits
    :param circuit_to_compile: input circuit one ones to transpile to s square-lattice layout
    :param seed: for transpiler optimization - for reproducibility of resulting transpiler circuit
    :return: returns transpiled circuit
    """

    device_topology = CouplingMap([[0, 1], [1, 0], [0, 2], [2, 0], [1, 3], [3, 1], [2, 3], [3, 2]])
    if n_qubits > 4:
        device_topology.add_edge(2, 4)
        device_topology.add_edge(4, 2)
    if n_qubits > 5:
        for i in [3, 4]:
            device_topology.add_edge(i, 5)
            device_topology.add_edge(5, i)
    if n_qubits > 6 or n_qubits < 4:
        raise NotImplementedError(f'Transpiler only implemented for 4, 5 and 6 qubits, not {n_qubits}.')

    # To get consistent transpilation results the seed is fixed.
    # Careful, the result might not be optimal!
    if seed is None:
        seed = 0
    trans_circ = transpile(circuit_to_compile, coupling_map=device_topology, optimization_level=3,
                           seed_transpiler=seed)
    return trans_circ


def create_noise_model(one_qubit_gate_error_rate: float = 0.0,
                       two_qubit_gate_error_rate: float = 0.0) -> NoiseModel:
    """
    Returns depolarizing noise channel for the QAOA

    :param one_qubit_gate_error_rate: error rate for a 1-qubit gate
    :param two_qubit_gate_error_rate: error rate for a 2-qubit gate
    :return: qiskit depolarizing noise channel
    """
    if one_qubit_gate_error_rate == 0.0 and two_qubit_gate_error_rate == 0.0:
        noise_model = None
    else:
        error_q1 = depolarizing_error(one_qubit_gate_error_rate, 1)
        error_q2 = depolarizing_error(two_qubit_gate_error_rate, 2)
        noise_model = NoiseModel()
        noise_model.add_all_qubit_quantum_error(error_q1, ['r', 'rz', 'id', 'u1', 'u2', 'u3'])
        noise_model.add_all_qubit_quantum_error(error_q2, ['cz', 'cx'])
    return noise_model


def get_groundstate(number_of_logical_qubits: int,
                    interactions: List[Tuple],
                    interaction_strengths: List[float]) -> List:
    """
    Returns the groundstate energy and the groundstate(s) of a logical problem graph

    :param number_of_logical_qubits: number of logical qubits
    :param interactions: The logical graph as an array of edges, for example [(0, 1), (1, 2), (2, 0)]
    :param interaction_strengths: Interaction strength, must be ordered according to 'logical_graph'.
    :return: the groundstate(s) of the system
    """

    number_of_possible_states = 2 ** number_of_logical_qubits

    configuration = [-1] * number_of_logical_qubits
    energy = sum([np.prod([configuration[i] for i in interactions[edge_index]]) * interaction_strengths[edge_index]
                  for edge_index in range(len(interactions))])
    et = energy

    min_energy_configurations = []
    pre = number_of_possible_states
    for i in range(0, number_of_possible_states):
        # find out which qubit to flip with the gray code
        c = number_of_possible_states + int(i / 2) ^ i
        i_flip = number_of_logical_qubits - len(bin(c ^ pre)) + 2  # +2 because of the '0b'-prefix
        configuration[i_flip] *= -1
        pre = c

        for ind in range(len(interactions)):
            if i_flip in interactions[ind]:
                et += 2 * np.prod([configuration[i] for i in interactions[ind]]) * interaction_strengths[ind]

        if abs(et - energy) < 1e-9:  # consider degeneracy
            energy = et
            min_energy_configurations += [configuration.copy()]
        elif et < energy:
            energy = et
            min_energy_configurations = [configuration.copy()]

    return min_energy_configurations


def create_complete_graph_and_parity_constraints(number_of_qubits: int) -> Tuple[List, List]:
    """
    creates a complete graph, described by the edges, and the parity constraint list

    :param number_of_qubits: number of qubits(nodes) in the logical graph
    :return: logical problem (complete) graph and the corresponding parity constraints
    """
    # create complete graph and corresponding dictionary
    dictionary_of_edges = {}
    edges_of_complete_graph = [(a, b) for a in range(number_of_qubits) for b in range(number_of_qubits) if a < b]
    for i, element in enumerate(edges_of_complete_graph):
        dictionary_of_edges[element] = i

    # create parity constraint list
    constraint_list = []
    indices = np.triu_indices(number_of_qubits - 1, 1)
    for qubit_1, qubit_2 in zip(indices[0], indices[1]):
        temporary_constraint = [(qubit_1, qubit_2), (qubit_1, qubit_2 + 1), (qubit_1 + 1, qubit_2 + 1)]
        if qubit_2 != qubit_1 + 1:
            temporary_constraint.append((qubit_1 + 1, qubit_2))
        constraint_list.append(temporary_constraint)

    return edges_of_complete_graph, [[dictionary_of_edges[c] for c in cs] for cs in constraint_list]
