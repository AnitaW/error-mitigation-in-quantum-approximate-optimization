# Error mitigation in parity quantum approximate optimization

This tutorial is based on the article [arXiv:2301.05042](https://arxiv.org/abs/2301.05042).
It shows how to simulate QAOA with the rerouting and parity approach 
for one problem instance, build on the open source library Qiskit.
There are small adaptions to the method used in the paper mentioned above: 
For simplicity, a different classical optimizer is used, also, here we use
the build in transpiler from qiskit (not tket from cambridge quantum computing). 
In this tutorial only the logical lines of the parity encoding are provided as 
spanning trees, but the user can insert spanning trees of their choice.
One can change the system size (number of logical qubits), 
QAOA layers, error rates etc. 
To reproduce the main results shown in the manuscript
insert stated parameters and sample sufficently many problem instances.

More information about the parity architecture an be found here:
Parity: https://www.science.org/doi/10.1126/sciadv.1500838,
Parity Compiler: https://doi.org/10.22331/q-2023-03-17-950
